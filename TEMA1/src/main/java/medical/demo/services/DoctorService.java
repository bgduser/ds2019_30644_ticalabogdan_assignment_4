package medical.demo.services;

import medical.demo.entities.Doctor;
import medical.demo.errorhandler.ResourceNotFoundException;
import medical.demo.repositories.DoctorRepository;
import medical.demo.validators.DoctorFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class DoctorService {

    @Autowired
    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }


    public Doctor findById(Integer id) {
        Optional<Doctor> doctor = doctorRepository.findById(id);

        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "id_doctor", id);
        }
        return doctor.get();
    }

    public List<Doctor> findAll() {

        return doctorRepository.getAllOrdered();


    }

    public Doctor insert(Doctor doctor) {

        DoctorFieldValidator.validateInsertOrUpdate(doctor);

        return doctorRepository.save(doctor);

    }


    public Integer update(Doctor doctor) {

        Optional<Doctor> doctorS = doctorRepository.findById(doctor.getId());

        if (!doctorS.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "doctor id", doctor.getId().toString());
        }

        DoctorFieldValidator.validateInsertOrUpdate(doctor);

        return doctorRepository.save(doctor).getId();
    }

    public void delete(Doctor doctor) {

        this.doctorRepository.deleteById(doctor.getId());
    }

}
