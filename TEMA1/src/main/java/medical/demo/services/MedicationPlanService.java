package medical.demo.services;

import medical.demo.entities.MedicationPlan;
import medical.demo.entities.Patient;
import medical.demo.errorhandler.ResourceNotFoundException;
import medical.demo.repositories.MedicationPlanRepository;
import medical.demo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MedicationPlanService {

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    private PatientRepository patientRepository;

    public MedicationPlan findById(Integer id) {
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(id);
        if (!medicationPlan.isPresent()) {
            throw new ResourceNotFoundException("Medication", "id_medication", id);
        }
        return medicationPlan.get();
    }

    public List<MedicationPlan> getMedicationPlansByIdPatient(int id) {
        Patient patient = patientRepository.findById(id).get();

        return medicationPlanRepository.getMedicationPlan(patient.getId());
    }
    public List<MedicationPlan> findAll() {
        return (medicationPlanRepository.findAll());
    }

    public void addMedicationInMedicationPlan(int idMedicationPlan,int idMedication){
        medicationPlanRepository.addMedicationInMedicationPlan(idMedicationPlan,idMedication);
    }


}
