package medical.demo.services;

import medical.demo.entities.Patient;
import medical.demo.errorhandler.ResourceNotFoundException;
import medical.demo.repositories.PatientRepository;
import medical.demo.validators.PatientFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public Patient findById(Integer id) {
        Optional<Patient> patient = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "id_patient", id);
        }
        return patient.get();
    }
//
//    public List<PatientDto> findAll() {
//        List<Patient> patients = patientRepository.getAllOrdered();
//
//        return patients.stream()
//                .map(PatientBuilder::generateDtoFromEntity)
//                .collect(Collectors.toList());
//    }
//
//
//
//
    public void insert(Patient patient) {

        PatientFieldValidator.validateInsertOrUpdate(patient);

        patientRepository
                .save(patient);
    }
    public List<Patient> findAll(){
        return patientRepository.findAll();
    }


    public void update(Patient patient) {

        Optional<Patient> patient1 = patientRepository.findById(patient.getId());

        if (!patient1.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", patient.getId().toString());
        }

        if(patient.getEmail()!=null && !(patient.getEmail().equals(""))){
            patient1.get().setEmail(patient.getEmail());
        }

        if(patient.getAddress()!=null && !(patient.getAddress().equals(""))){
            patient1.get().setAddress(patient.getAddress());
        }

        if(patient.getPassword()!=null && !(patient.getPassword().equals(""))){
            patient1.get().setPassword(patient.getPassword());
        }

        PatientFieldValidator.validateInsertOrUpdate(patient1.get());
        patientRepository.save(patient1.get());
    }

    public void delete(Patient patient) {
        this.patientRepository.deleteById(patient.getId());
    }


}
