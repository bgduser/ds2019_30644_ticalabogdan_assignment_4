package medical.demo.controller;


import medical.demo.entities.Medication;
import medical.demo.entities.MedicationPlan;
import medical.demo.repositories.MedicationPlanRepository;
import medical.demo.repositories.MedicationRepository;
import medical.demo.services.MedicationPlanService;
import medical.demo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {
    @Autowired
    MedicationPlanService medicationPlanService;

    @Autowired
    MedicationService medicationService;


    public ResponseEntity<List<MedicationPlan>> getMedicationPlansByIdPatient(@PathVariable("id") int id) {

        List<MedicationPlan> medicationPlans = medicationPlanService.getMedicationPlansByIdPatient(id);

        return new ResponseEntity<>(medicationPlans, HttpStatus.OK);
    }


    @GetMapping("/signup")
    public String showSignUpForm(Medication medication) {
        return "add-medicationPlan";
    }

    @RequestMapping("/home")
    public String showMedicationPlanHome(Model model) {

        model.addAttribute("listOfMedicationPlans", medicationPlanService.findAll());
        return "index";
    }

    @GetMapping("/")
    public ResponseEntity<List<MedicationPlan>> getAll(){
        return new ResponseEntity<>(medicationPlanService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/medicament/{idMedicationPlan}/{idMedication}")
    public ResponseEntity<Void> addMedicationInMedicationPlan(@PathVariable("idMedicationPlan") int idMedicationPlan,@PathVariable("idMedication") int idMedication){

        if(medicationPlanService.findById(idMedicationPlan)==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(medicationService.findById(idMedication)==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        medicationPlanService.addMedicationInMedicationPlan(idMedicationPlan,idMedication);

        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
