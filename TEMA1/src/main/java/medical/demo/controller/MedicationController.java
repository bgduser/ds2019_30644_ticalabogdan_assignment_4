package medical.demo.controller;


import medical.demo.entities.Medication;
import medical.demo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/medication")
public class MedicationController {

    @Autowired
    MedicationService medicationService;

    @GetMapping("/")
    public ResponseEntity<List<Medication>> getAll(){
        return new ResponseEntity<>(medicationService.findAll(), HttpStatus.OK);
    }


    @GetMapping("/signup")
    public String showSignUpForm(Medication medication) {
        return "add-medication";
    }

    @RequestMapping("/home")
    public String showMedicationHome(Model model) {

        model.addAttribute("listOfMedications", medicationService.findAll());
        return "index";
    }

    @PostMapping("/addMedication")
    public String addMedication(@Valid Medication medication, BindingResult result, Model model) {
        medicationService.insert(medication);
        model.addAttribute("listOfMedications", medicationService.findAll());

        return "index";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        Medication medication = medicationService.findById(id);
        model.addAttribute("medication", medication);
        return "update-medication";
    }

    @PostMapping("/update/{id}")
    public String updateMedication(@PathVariable("id") int id, @Valid Medication medication, BindingResult result, Model model) {
        if (result.hasErrors()) {
            medication.setId(id);
            return "update-medication";
        }

        medicationService.insert(medication);
        model.addAttribute("listOfMedications", medicationService.findAll());
        return "index";
    }

    @RequestMapping(value = "/delete/{id}",method = RequestMethod.GET)
    public String deleteMedication(@PathVariable("id") int id, Model model) {
        Medication medication = medicationService.findById(id);
        medicationService.delete(medication);
        model.addAttribute("listOfMedications", medicationService.findAll());
        return "index";
    }

}
