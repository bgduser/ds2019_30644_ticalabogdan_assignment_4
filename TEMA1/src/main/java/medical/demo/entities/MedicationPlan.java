package medical.demo.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "medication_plan")
public class MedicationPlan implements Serializable {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    @Column(name = "daily_interval")
    private String dailyInterval;

    @Column(name = "period_treatment")
    private String periodOfTreatment;

    @Column(name = "medicament")
    private String medicament;

    @Column(name = "status")
    private String  status;


    @ManyToMany(mappedBy = "listOfMedicationPlan")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Column(name = "medications")
    @JsonIgnoreProperties("listOfMedicationPlan")
    private Set<Medication> medications;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(name = "patient_medication_plan",
            joinColumns = @JoinColumn(name = "medication_plan_id"),
            inverseJoinColumns = @JoinColumn(name = "patient_id"))
    @JsonIgnoreProperties("listOfMedicationPlan")
    private Set<Patient> patients = new HashSet<>();




    public MedicationPlan() {

    }

    public MedicationPlan(String dailyInterval, String periodOfTreatment, Set<Medication> medications, Set<Patient> patients) {
        this.dailyInterval = dailyInterval;
        this.periodOfTreatment = periodOfTreatment;
        this.medications = medications;
        this.patients = patients;
    }

    public MedicationPlan(String dailyInterval, String periodOfTreatment, String medicament, Set<Patient> patients) {
        this.dailyInterval = dailyInterval;
        this.periodOfTreatment = periodOfTreatment;
        this.medicament = medicament;
        this.patients = patients;
    }

    public MedicationPlan(String dailyInterval, String periodOfTreatment, String medicament, String status) {
        this.dailyInterval = dailyInterval;
        this.periodOfTreatment = periodOfTreatment;
        this.medicament = medicament;
        this.status = status;
    }

    @Override
    public String toString() {
        return "MedicationPlan{" +
                "id=" + id +
                ", dailyInterval='" + dailyInterval + '\'' +
                ", periodOfTreatment='" + periodOfTreatment + '\'' +
                ", medications=" + medications +
                ", patients=" + patients +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public String getPeriodOfTreatment() {
        return periodOfTreatment;
    }

    public void setPeriodOfTreatment(String periodOfTreatment) {
        this.periodOfTreatment = periodOfTreatment;
    }

    public Set<Medication> getMedicaments() {
        return medications;
    }

    public String getMedicament() {
        return medicament;
    }

    public void setMedicament(String medicament) {
        this.medicament = medicament;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    public List<Medication> getMedicationFromMedicationList() {
        List<Medication> medicationsList = new ArrayList<>();

        Iterator<Medication> iterator = medications.iterator();
        while (iterator.hasNext()) {
            medicationsList.add(iterator.next());
        }

        return medicationsList;
    }


}
