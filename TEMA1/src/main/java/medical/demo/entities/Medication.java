package medical.demo.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "medication")
public class Medication {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "effects", length = 200)
    private String effects;

    @Column(name = "dosage", length = 10)
    private String dosage;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(name="medication_plan_medication",
            joinColumns = @JoinColumn(name="medication_id"),
            inverseJoinColumns = @JoinColumn(name="medication_plan_id"))
    @JsonIgnoreProperties("medications")
    private Set<MedicationPlan> listOfMedicationPlan=new HashSet<>();

    public Medication() {
    }

    public Medication(int id, String name, String effects, String dosage,Set<MedicationPlan> listOfMedicationPlan) {
        this.id = id;
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
        this.listOfMedicationPlan = listOfMedicationPlan;
    }

    public Medication(String name, String effects, String dosage) {
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffects() {
        return effects;
    }

    public void setEffects(String effects) {
        this.effects = effects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medication that = (Medication) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(effects, that.effects) &&
                Objects.equals(dosage, that.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, effects, dosage);
    }
}
