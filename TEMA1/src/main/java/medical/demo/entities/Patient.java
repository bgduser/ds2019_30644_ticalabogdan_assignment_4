package medical.demo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.AUTO;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient {
    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birth_date")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date birthDate;


    @Column(name = "address")
    private String address;

    @Column(name = "recomandation", length = 100)
    private String recomandation;

    @Column(name = "email", length = 200)
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_doctor",  nullable = true)
    @JsonIgnoreProperties("patients")
    //@JsonBackReference
    private Doctor doctor;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_caregiver",  nullable = true)
    @JsonIgnoreProperties("patients")
    private Caregiver caregiver;

    @ManyToMany(mappedBy = "patients", fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Column(name = "medication_plan")
    @JsonIgnoreProperties("patients")
    private Set<MedicationPlan> listOfMedicationPlan = new HashSet<>();


    public Patient() {

    }

    public Patient(String name, Date birthDate, String address, String recomandation, String email, String password, Caregiver caregiver, Doctor doctor,Set<MedicationPlan> listOfMedicationPlan) {
        this.name = name;
        this.birthDate = birthDate;
        this.address = address;
        this.recomandation = recomandation;
        this.email = email;
        this.password = password;
        this.caregiver = caregiver;
        this.doctor = doctor;
        this.listOfMedicationPlan=listOfMedicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecomandation() {
        return recomandation;
    }

    public void setMedicalRecomandation(String recomandation) {
        this.recomandation = recomandation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public Set<MedicationPlan> getListOfMedicationPlan() {
        return listOfMedicationPlan;
    }

    public void setListOfMedicationPlan(Set<MedicationPlan> listOfMedicationPlan) {
        this.listOfMedicationPlan = listOfMedicationPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(id, patient.id) &&
                Objects.equals(name, patient.name) &&
                Objects.equals(birthDate, patient.birthDate) &&
                Objects.equals(address, patient.address) &&
                Objects.equals(recomandation, patient.recomandation) &&
                Objects.equals(email, patient.email) &&
                Objects.equals(password, patient.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthDate, address, recomandation, email, password);
    }
}
