package medical.demo.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "caregiver")
public class Caregiver {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthDate")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date birthDate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;


    @Column(name = "email", length = 200)
    private String email;

    @Column(name = "password")
    private String password;
//, cascade = CascadeType.ALL
    @OneToMany(mappedBy = "caregiver", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties("caregiver")
    // @JsonManagedReference
    private Set<Patient> patients;

    public Caregiver() {
    }

    public Caregiver(String name, Date birthDate, String gender, String address, String email, String password, Set<Patient> patients) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.email = email;
        this.password = password;
        this.patients = patients;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Caregiver caregiver = (Caregiver) o;
        return Objects.equals(id, caregiver.id) &&
                Objects.equals(name, caregiver.name) &&
                Objects.equals(birthDate, caregiver.birthDate) &&
                Objects.equals(gender, caregiver.gender) &&
                Objects.equals(address, caregiver.address) &&
                Objects.equals(email, caregiver.email) &&
                Objects.equals(password, caregiver.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthDate, gender, address, email, password);
    }
}
