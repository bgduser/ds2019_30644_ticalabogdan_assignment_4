package medical.demo.repositories;

import medical.demo.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

    @Query(value = "SELECT u " +
            "FROM Doctor u " +
            "ORDER BY u.name")
    List<Doctor> getAllOrdered();

    @Query(value = "SELECT p " +
            "FROM Doctor p " +
            "INNER JOIN FETCH p.patients i"
    )
    List<Doctor> getAllFetch();
}