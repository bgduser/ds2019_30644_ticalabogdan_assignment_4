package medical.demo.repositories;

import medical.demo.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {

    @Transactional
    @Modifying
    @Query(value = "INSERT into medication_medication_plan  (medication_id,medication_plan_id) VALUES (:idMedication,:idMedicationPlan) ",nativeQuery = true)
    void addMedicationInMedicationPlan(@Param("idMedicationPlan") int idMedicationPlan, @Param("idMedication") int idMedication);

    @Transactional
    @Query(value = "SELECT * FROM medication_plan m JOIN patient_medication_plan pm ON m.id=pm.medication_plan_id AND pm.patient_id= ?1  ",
            nativeQuery = true)
    ArrayList<MedicationPlan> getMedicationPlan(int idPatient);
}
