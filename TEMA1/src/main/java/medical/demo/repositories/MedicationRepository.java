package medical.demo.repositories;

import medical.demo.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MedicationRepository extends JpaRepository<Medication, Integer> {

    @Query(value = "Select u "+
            "FROM Medication u "+
            "ORDER BY u.name")
    List<Medication> getAllOrdered();
}
