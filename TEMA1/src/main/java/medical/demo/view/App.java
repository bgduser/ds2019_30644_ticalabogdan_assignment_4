package view;

import client.Client;
import model.MedicationPlan;
import rmi.DemoRemote;
import rmi.Implementation;

import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.GregorianCalendar;
import java.util.List;


import javax.swing.*;


public class App {

    public JFrame frame;
    private static JTable tableMedication;
    JLabel lblDateAndTime = new JLabel();

    public void Clock() {
        Thread clock = new Thread() {

            public void run() {
                try {
                    for(;;) {

                        GregorianCalendar calendar = new GregorianCalendar();

                        lblDateAndTime.setText("" + calendar.getTime());

                        sleep(1000);
                    }

                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        };

        clock.start();

    }

    public App() {

        Clock();

        frame = new JFrame();
        frame.setBounds(100, 100, 931, 448);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        JButton btnDownload = new JButton("Download MedicationPlan");
        btnDownload.setBounds(24, 363, 244, 25);
        btnDownload.setFont(new Font("Tahoma", Font.BOLD, 15));
        btnDownload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                tableMedication.repaint();
            }
        });
        frame.getContentPane().setLayout(null);
        frame.getContentPane().add(btnDownload);

        JButton btnTaken = new JButton("Taken");
        btnTaken.setBounds(24, 105, 227, 36);
        btnTaken.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Setting status >> medication taken!");

            }
        });
        btnTaken.setFont(new Font("Tahoma", Font.BOLD, 15));
        frame.getContentPane().add(btnTaken);

        JButton btnNotTaken = new JButton("Not Taken");
        btnNotTaken.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JOptionPane.showMessageDialog(null, "Setting status >> medication not taken!");

            }
        });

        btnNotTaken.setFont(new Font("Tahoma", Font.BOLD, 14));
        btnNotTaken.setBounds(24, 144, 227, 36);
        frame.getContentPane().add(btnNotTaken);

        JLabel lblMedications = new JLabel("Medication Plans");
        lblMedications.setBounds(468, 47, 157, 16);
        lblMedications.setFont(new Font("Tahoma", Font.BOLD, 16));
        frame.getContentPane().add(lblMedications);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(278, 105, 598, 251);
        frame.getContentPane().add(scrollPane);

        tableMedication = new JTable();
        tableMedication.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }
        });


        lblDateAndTime.setFont(new Font("Consolas", Font.ITALIC, 14));
        lblDateAndTime.setBounds(24, 48, 314, 16);
        frame.getContentPane().add(lblDateAndTime);

        scrollPane.setViewportView(tableMedication);



    }
}