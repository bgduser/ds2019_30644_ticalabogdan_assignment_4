package medical.demo;

import medical.demo.entities.MedicationPlan;
import medical.demo.soap.Hello;
import medical.demo.view.App;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@SpringBootApplication
public class PlatformApplication {

    public static Hello hello;


    public static void main(String[] args) throws MalformedURLException {


        SpringApplication.run(PlatformApplication.class, args);

        System.setProperty("java.awt.headless", "false");

        URL url = new URL("http://localhost:8082/services/hello?wsdl");

        QName qname = new QName("http://soap.demo.medical/",
                "HelloImplService");

        Service service = Service.create(url, qname);
        hello = service.getPort(Hello.class);

        List<MedicationPlan> list = hello.getMedicationPlan(2);
        System.out.println("Size list" +  list.size());

        App view = new App();
        view.setVisible(true);


    }


}
