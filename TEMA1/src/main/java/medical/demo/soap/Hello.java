package medical.demo.soap;


import medical.demo.entities.MedicationPlan;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC, use = SOAPBinding.Use.LITERAL)
public interface Hello {


    @WebMethod
    void addRecommandation(int idPatient, String recomandation, int idDoctor);

    @WebMethod
    ArrayList<MedicationPlan> getMedicationPlan(int patientId);

}
