package medical.demo.validators;

import medical.demo.entities.Patient;
import medical.demo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class PatientFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(PatientFieldValidator.class);
    private static final EmailValidator EMAIL_VALIDATOR = new EmailValidator();

    public static void validateInsertOrUpdate(Patient patient) {

        List<String> errors = new ArrayList<>();
        if (patient == null) {
            errors.add("patientDto is null");
            throw new IncorrectParameterException(Patient.class.getSimpleName(), errors);
        }
        if (patient.getEmail() == null || !EMAIL_VALIDATOR.validate(patient.getEmail())) {
            errors.add("Patient email has invalid format");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PatientFieldValidator.class.getSimpleName(), errors);
        }
    }
}
