package medical.demo.validators;

import medical.demo.entities.Medication;
import medical.demo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class MedicationFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(MedicationFieldValidator.class);
    private static final EmailValidator EMAIL_VALIDATOR = new EmailValidator();

    public static void validateInsertOrUpdate(Medication medication) {

        List<String> errors = new ArrayList<>();
        if (medication == null) {
            errors.add("medicationDto is null");
            throw new IncorrectParameterException(Medication.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(MedicationFieldValidator.class.getSimpleName(), errors);
        }
    }
}
