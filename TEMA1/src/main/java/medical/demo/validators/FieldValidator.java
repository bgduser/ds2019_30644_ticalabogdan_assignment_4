package medical.demo.validators;

public interface FieldValidator<T> {
    boolean validate(T t);
}
