package medical.demo.config;

import javax.xml.ws.Endpoint;

import medical.demo.soap.Hello;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringSoapConfig {

    @Autowired
    private Bus bus;

    @Autowired
    private Hello hello;

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, hello);
        endpoint.publish("/hello");
        return endpoint;
    }

}
