package medical.demo.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "medication_plan")
public class MedicationPlan implements Serializable {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    @Column(name = "daily_interval")
    private String dailyInterval;

    @Column(name = "period_treatment")
    private String periodOfTreatment;

    @Column(name = "medicament")
    private String medicament;


    @Column(name = "status")
    private String status;


    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(name = "patient_medication_plan",
            joinColumns = @JoinColumn(name = "medication_plan_id"),
            inverseJoinColumns = @JoinColumn(name = "patient_id"))
    @JsonIgnoreProperties("listOfMedicationPlan")
    private List<Patient> patients = new ArrayList<>();


    public MedicationPlan() {

    }

    public MedicationPlan(String dailyInterval, String periodOfTreatment, String medicament, List<Patient> patients) {
        this.dailyInterval = dailyInterval;
        this.periodOfTreatment = periodOfTreatment;
        this.medicament = medicament;
        this.patients = patients;
    }

    public MedicationPlan(String dailyInterval, String periodOfTreatment, String medicament, String status) {
        this.dailyInterval = dailyInterval;
        this.periodOfTreatment = periodOfTreatment;
        this.medicament = medicament;
        this.status = status;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public String getPeriodOfTreatment() {
        return periodOfTreatment;
    }

    public void setPeriodOfTreatment(String periodOfTreatment) {
        this.periodOfTreatment = periodOfTreatment;
    }


    public String getMedicament() {
        return medicament;
    }

    public void setMedicament(String medicament) {
        this.medicament = medicament;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }


}
