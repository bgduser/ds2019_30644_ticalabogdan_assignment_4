package medical.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoctorWebServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(DoctorWebServiceApplication.class, args);
	}

}
