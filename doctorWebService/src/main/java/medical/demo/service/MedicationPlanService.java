package medical.demo.service;

import medical.demo.entity.MedicationPlan;
import medical.demo.repository.MedicationPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MedicationPlanService {

    @Autowired
    MedicationPlanRepository medicationPlanRepository;

    public ArrayList<MedicationPlan> getMedicationPlanById(int idPatient) {

        return medicationPlanRepository.getMedicationPlan(idPatient);
    }

}
