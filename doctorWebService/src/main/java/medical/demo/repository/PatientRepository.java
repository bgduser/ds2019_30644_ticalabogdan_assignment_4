package medical.demo.repository;


import medical.demo.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE patient SET recomandation = :recomandation WHERE id = :id and id_doctor = :id_doctor", nativeQuery = true)
    void addRecomandationforPatient(@Param("recomandation") String recomandation, @Param("id") int id, @Param("id_doctor") int id_doctor);
}