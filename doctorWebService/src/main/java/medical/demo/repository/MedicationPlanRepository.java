package medical.demo.repository;

import medical.demo.entity.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {

    @Transactional
    @Query(value = "SELECT * FROM medication_plan m JOIN patient_medication_plan pm ON m.id=pm.medication_plan_id AND pm.patient_id= ?1  ",
            nativeQuery = true)
    ArrayList<MedicationPlan> getMedicationPlan(int idPatient);
}
