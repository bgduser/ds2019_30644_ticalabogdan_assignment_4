package medical.demo.soap;

import medical.demo.entity.MedicationPlan;
import medical.demo.entity.Patient;
import medical.demo.repository.MedicationPlanRepository;
import medical.demo.repository.PatientRepository;
import medical.demo.service.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@WebService(endpointInterface = "medical.demo.soap.Hello")
public class HelloImpl implements Hello {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private MedicationPlanService medicationPlanService;

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;


    @Override
    public void addRecommandation(int idPatient, String recomandation, int idDoctor) {
        Optional<Patient> patient = patientRepository.findById(idPatient);

        if (patient.isPresent()) {
            patientRepository.addRecomandationforPatient(recomandation, idPatient, idDoctor);
        }
    }


    @Override
    public ArrayList<MedicationPlan> getMedicationPlan(int patientId) {
        return medicationPlanService.getMedicationPlanById(patientId);


    }
}

